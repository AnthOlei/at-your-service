from atyourservice import db, login_manager, app
from flask_login import UserMixin
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask import app
from datetime import datetime
#MY API KEY AIzaSyAqvTk3yx0UU5ecmVT_Ztri4ttHbTrxCWk

class JobPost(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    title = db.Column(db.String(80), nullable=False)
    job_description = db.Column(db.Text, nullable=False)
    price = db.Column(db.Float(100), nullable = False)
    area_name = db.Column(db.String(120), unique=False, nullable=False)
    latitude = db.Column(db.Float(100), nullable = True)
    longitude = db.Column(db.Float(100), nullable = True)
    time = db.Column(db.String(80), nullable = True)
    filled = db.Column(db.DateTime, nullable=True)
    verified = db.Column(db.Boolean, default = False, nullable = False)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    taken_by = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=True) 

class RemovedPost(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    price = db.Column(db.Float(100), nullable = False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)

class Transaction(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    amount = db.Column(db.Float(120), unique=False, nullable=False)
    job_title = db.Column(db.String(80), nullable=True)
    transaction_date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

    from_id = db.Column(db.Integer, db.ForeignKey('user.id'), unique=False, nullable=True)
    to_id = db.Column(db.Integer, db.ForeignKey('user.id'), unique=False, nullable=True)

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)

class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(80), nullable=True)
    last_inital = db.Column(db.String(80), nullable=True)
    email = db.Column(db.String(120), unique=True, nullable=False)
    verified = db.Column(db.Boolean, default = False, nullable = False)
    money = db.Column(db.Float(100), default = 0, nullable = True)
    latitude = db.Column(db.Float(100), nullable = True)
    longitude = db.Column(db.Float(100), nullable = True)
    area_name = db.Column(db.String(120), unique=False, nullable=True)
    password = db.Column(db.String(120), unique = False, nullable = False)

    job_posts = db.relationship("JobPost", foreign_keys=[JobPost.user_id], lazy=True)
    job_taken = db.relationship("JobPost", foreign_keys=[JobPost.taken_by], lazy=True)
    money_earned = db.relationship("Transaction", foreign_keys=[Transaction.to_id], lazy=True)
    money_spent = db.relationship("Transaction", foreign_keys=[Transaction.from_id], lazy=True)

    def get_reset_token(self, expires_sec = 1800):
        s = Serializer(app.config['SECRET_KEY'], expires_sec)
        return s.dumps({'user_id': self.id}).decode('utf-8')

    @staticmethod
    def verify_reset_token(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            user_id = s.loads(token)['user_id']
        except:
            return None
        return User.query.get(user_id)
    