from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
from flask_mail import Mail
from atyourservice.config import Config

app = Flask(__name__)

@app.context_processor
def inject_globals():
    return dict(app_name="At Your Service", server_name="localhost:5000")

app.config.from_object(Config)
db = SQLAlchemy(app)
mail = Mail(app)
bcrypt = Bcrypt(app)
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'site.login'

db.init_app(app)
bcrypt.init_app(app)

from atyourservice.errors.handlers import errors
from atyourservice.site.routes import site 
from atyourservice.legal.routes import legal

app.register_blueprint(errors)
app.register_blueprint(site)
app.register_blueprint(legal)