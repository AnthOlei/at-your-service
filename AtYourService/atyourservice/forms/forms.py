from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField, TextAreaField, DecimalField
from wtforms.validators import DataRequired, Length, Email, EqualTo
from wtforms.widgets import TextArea

class RegistrationForm(FlaskForm):
    email = StringField('Email address', [DataRequired(), Email()])
    password = PasswordField('New Password', [
        Length(min=3),
        DataRequired(),
        EqualTo('confirm_password', message='Passwords must match.')
    ])
    confirm_password = PasswordField('Repeat Password')
    submit = SubmitField('Sign Up')

class LoginForm(FlaskForm):
    email = StringField('Email address', [DataRequired(), Email()])
    password = PasswordField('Password', [
        DataRequired(),
    ])
    remember = BooleanField('Remember me?')
    submit = SubmitField('Log In')

class PostJobForm(FlaskForm):
    title = StringField('Title', [DataRequired()])
    location = StringField('Location', [DataRequired()])
    price = DecimalField('Price', [DataRequired()])
    time = StringField('Time', [DataRequired()])
    description = TextAreaField(u'Description')

    submit = SubmitField('Post Job')

class AddMoneyForm(FlaskForm):
    amount = DecimalField('Price', [DataRequired()])
    submit = SubmitField('Add Money')