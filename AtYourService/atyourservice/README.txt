# At Your Service

At your service is a website mock-up developed fullstack by Anthony Oleinik. 
Users post and take small jobs around the neighborhood. 

## Motication
Developed mockup for startup "At Your Service" for use in pitches to VC.

## Technology

Frontend: Bootstrap 4, CSS grid, jquery
Backend: Flask, MySQL, Linux Ubuntu, Nginx and Guincorn

Linode webhosting: Linode is a remote Linux server running Ubuntu.
Uncomplicated Firewall for firewall protection.

##Features

User login system, internal API calls for dynamic page updates.

## How to use?

Either: 
Navigate a browser to 172.105.26.236, the DNS where the mockup is hosted.

Or:
Download the repo, navigate to root file directory and run:
$source ays/bin/activate
$python3 run.py
A local server should be opened up at localhost:5000.

## Contact

antholeinik@gmail.com

## License
[MIT](https://choosealicense.com/licenses/mit/)