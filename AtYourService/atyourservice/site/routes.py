from flask import Blueprint, render_template, jsonify, redirect, url_for, flash, request, make_response
from atyourservice import db, bcrypt
from atyourservice.forms.forms import AddMoneyForm, LoginForm, PostJobForm, RegistrationForm
from models import User, JobPost, Transaction, RemovedPost
from flask_login import current_user, login_user, logout_user, login_required
from datetime import datetime
import requests

site = Blueprint('site', __name__)


@site.route('/', methods=["GET", "POST"])
def index():
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user:
            flash("there is already a user with this email.", "warning")
            return render_template('index.html', title="At Your Service!", form=form)
        hashed_password = bcrypt.generate_password_hash(
            form.password.data).decode('utf-8')
        user = User(email=form.email.data, password=hashed_password)
        db.session.add(user)
        db.session.commit()
        login_user(user)
        flash("Account Created!", 'success')
        return redirect(url_for('site.home'))
    return render_template('index.html', title="At Your Service!", form=form)


@site.route("/logout/")
def logout():
    logout_user()
    flash("you've been logged out!", "warning")
    return redirect(url_for('site.index'))

@site.route("/aboutUs/")
def aboutUs():
    return render_template('aboutUs.html', title="At Your Service!")


@site.route('/login/', methods=["GET", "POST"])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('site.home'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user, remember=form.remember.data)
            next_page = request.args.get('next')
            return redirect(next_page) if next_page else redirect(url_for('site.home'))
        else:
            flash('Login Unsuccessful. Please check email and password.', 'warning')
    else:
        print("not validated")
    return render_template('login.html', title="At Your Service!", form_name="Login", form=form)


@site.route('/register/', methods=["GET", "POST"])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('site.home'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user:
            flash("there is already a user with this email.", "warning")
            return render_template('index.html', title="At Your Service!", form=form)
        hashed_password = bcrypt.generate_password_hash(
            form.password.data).decode('utf-8')
        user = User(email=form.email.data, password=hashed_password)
        db.session.add(user)
        db.session.commit()
        login_user(user)
        flash("Account Created!", 'success')
        return redirect(url_for('site.home'))
    return render_template('register.html', title="At Your Service!", form=form, form_name="Register")


@site.route('/home/', methods=["GET", "POST"])
@login_required
def home():
    form = PostJobForm()
    if form.validate_on_submit():
        if float(form.price.data) < 0:
            flash("Please make sure your price is a positive value.", "danger")
            return redirect(url_for('site.home'))
        else:
            post = JobPost(title=form.title.data, job_description=form.description.data,
                           area_name=form.location.data, price=form.price.data, user_id=current_user.id, time=form.time.data)
            db.session.add(post)
            db.session.commit()
            flash(
                "Job posted! an admin will verify your post and it will be made public.", "success")
            return redirect(url_for('site.home'))
    return render_template('home.html', title="At Your Service!", form=form)

# DO NOT change these urls; finicky integration with javascript post paging.
# If you change these, make sure you know what you're doing!
@site.route('/take_job/')
def takeJob_():
    return redirect(url_for('site.home'))


@site.route('/take_job/<int:job_num>/')
def takeJob(job_num):
    if not current_user.is_authenticated:
        flash("You cannot take jobs unless you are logged in.", "error")
        return redirect(url_for("site.index"))
    post = JobPost.query.filter_by(id=job_num).first()
    if post.taken_by:
        flash("sorry! This job was taken while you were browsing.", "error")
        return redirect((url_for("site.home")))
    post.taken_by = current_user.id
    db.session.commit()
    flash("Job taken! Their contact info can be found below by clicking on that job.", "success")
    return(redirect(url_for('site.inspectUser')))
    return job_num
# end cautiousness.


@site.route('/user/')
@login_required
def inspectUser():
    # show jobs taken
    taken = JobPost.query.filter_by(taken_by=current_user.id).all()
    posted = JobPost.query.filter_by(user_id=current_user.id).all()
    return render_template('inspectUser.html', title='At Your Service!', taken=taken, posted=posted)


@site.route('/about/')
def about():
    return render_template('inspectUser.html', title='At Your Service!',)


@site.route('/transfer/')
@login_required
def transferOut():
    return render_template('transferOut.html', title='At Your Service!',)


@site.route('/admin/')
@login_required
def admin():
    if current_user.email != "danny.oleinik@atyourservice.business":
        flash("you need a higher rank to acess this page.", "error")
        flash(current_user.email)
        return redirect(url_for("site.index"))
    flash("Welcome, Bobby!", "success")
    posted = JobPost.query.filter_by(taken_by=None).all()
    refunded = RemovedPost.query.join(User, User.id == RemovedPost.user_id).add_columns(RemovedPost.price, User.email).all()
    paid = JobPost.query.filter(JobPost.filled != None).join(User, User.id == JobPost.user_id).add_columns(JobPost.price, User.email).all()
    return render_template('admin.html', title='At Your Service!', posted=posted, refunded=refunded, paid=paid)


@site.route('/admin/verify/')
@login_required
def verify():
    if current_user.email != "danny.oleinik@atyourservice.business":
        flash("you need a heigher rank to acess this page. Are you Bobby?", "error")
        return redirect(url_for("site.index"))
    flash("Welcome, Bobby!", "success")
    posts = JobPost.query.filter_by(verified=False).all()
    return render_template('adminVerify.html', title='At Your Service!', posts=posts)


@site.route('/admin/verify/<int:id>/')
def verifyPost(id):
    if current_user.email != "danny.oleinik@atyourservice.business":
        flash("this route is only for admins", "error")
        return redirect(url_for('site.home'))
    post = JobPost.query.filter_by(id=id).first()
    post.verified = True
    db.session.commit()
    flash('post successfully verifed.', 'success')
    return redirect(url_for('site.verify'))


@site.route('/admin/<int:id>/')
def removePost(id):
    if current_user.email == "danny.oleinik@atyourservice.business":
        post = JobPost.query.filter_by(id=id).first()
        user = User.query.filter_by(id=JobPost.user_id).first()
        db.session.delete(post)
        db.session.commit()
        flash('post successfully deleted.', 'warning')
        return redirect(url_for('site.admin'))
    flash("this route is only for admins", "error")
    return redirect(url_for('site.home'))


@site.route('/getPosts/', methods=["GET", "POST"])
def getPosts():
    posts = JobPost.query.all()
    response = dict()
    for post in posts:
        response.update({"post": {
            "title": post.title,
            "description": post.job_description,
            "price": post.price,
            "area": post.area_name,
            "time": post.time,
            "id": post.id,
            "poster_id": post.user_id,
            "verified": post.verified,
            "taken_by": post.taken_by,
        }})
    return jsonify(response)


@site.route('/getPostForModal/taken/<int:post_id>/')
def getModalTaken(post_id):
    post = JobPost.query.filter_by(id=post_id).join(User, User.id == JobPost.user_id).add_columns(
        JobPost.area_name, JobPost.job_description, JobPost.price, JobPost.title, User.email).first()
    return jsonify({
        "title": post.title,
        "location": post.area_name,
        "description": post.job_description,
        "price": post.price,
        "posted_by": post.email,
    })

@site.route('/removeJob/')
def removeJob_():
    return redirect(url_for("site.inspectUser"))

@site.route('/removeJob/<int:post_id>/')
def removeJob(post_id):
    post = JobPost.query.filter_by(id = post_id).first()
    if post.user_id != current_user.id:
        flash("This post was not posted by you.", "danger")
        return redirect(url_for("site.inspectUser"))
    if not post:
        flash("post not found.", "danger")
        return redirect(url_for("site.inspectUser"))
    if post.taken_by != None:
        flash("this post has been taken.", "danger")
        return redirect(url_for("site.inspectUser"))
    JobPost.query.filter_by(id = post_id).delete()
    db.session.add(RemovedPost(user_id = current_user.id, price = post.price))
    db.session.commit()
    flash("post marked as complete! Their payment will be transfered shortly." , "success")

@site.route('/markAsComplete/')
def markComplete_():
    return redirect(url_for("site.inspectUser"))

@site.route('/markAsComplete/<int:post_id>/')
def markComplete(post_id):
    post = JobPost.query.filter_by(id = post_id).first()
    if post.user_id != current_user.id:
        flash("This post was not posted by you.", "danger")
        return redirect(url_for("site.inspectUser"))
    if not post:
        flash("post not found.", "danger")
        return redirect(url_for("site.inspectUser"))
    if post.taken_by == None:
        flash("this post has not been taken yet.", "danger")
        return redirect(url_for("site.inspectUser"))
    post.filled = datetime.utcnow()
    db.session.commit()
    flash("post marked as complete! Their payment will be transfered shortly." , "success")
    return redirect(url_for("site.inspectUser"))
        

@site.route('/getPostForModal/posted/<int:post_id>/')
def getModalPost(post_id):
    post = JobPost.query.filter_by(id=post_id).first()
    taken = User.query.filter_by(id = post.taken_by).first()
    return_me = {
        "title": post.title,
        "location": post.area_name,
        "description": post.job_description,
        "price": post.price,
    }
    if taken:
        return_me.update({"taken_by": taken.email})
    else:
        return_me.update({"taken_by": "Nobody"})

    if post.filled:
        return_me.update({"filled": True})
    else:
        return_me.update({"filled": False})
    return jsonify(return_me)


@site.route('/getPost/<int:post_num>/', methods=["GET", "POST"])
def getPost(post_num):
    post = JobPost.query.filter_by(taken_by=None).filter_by(
        verified=True).offset(post_num - 1).first()
    if not post:
        return make_response(jsonify({"error": "no post"}))
    response = dict()
    response.update({"post": {
        "title": post.title,
        "description": post.job_description,
        "price": post.price,
        "area": post.area_name,
        "time": post.time,
        "id": post.id,
        "poster_id": post.user_id,
    }})
    if "max" in request.values:
        response.update({
            "max": JobPost.query.filter_by(taken_by=None).count(),
        })
    return make_response(jsonify(response))
