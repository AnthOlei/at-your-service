const apiURL = "https://atyourservice.business";

async function updateJobModal(job_id){
    resetJobModal();
    let json = await requestJobPost(job_id);
    displayJobUpdate(json, job_id);
}

const requestJobPost = async (job_id) => {
    const response = await fetch(apiURL + '/getPostForModal/taken/' + job_id + '/');
    const json = await response.json();
    return json;
}

function displayJobUpdate(json, job_id){
    $( "#taken-title" ).text(json["title"]);
    document.getElementById("taken-location").innerHTML = json["location"];
    document.getElementById("taken-description").innerHTML = json["description"];
    document.getElementById("taken-price").innerHTML = json["price"];
    document.getElementById("taken-taken").innerHTML = json["posted_by"];
}

function resetJobModal(){
    $( "#taken-title" ).text("Loading...");
    document.getElementById("taken-location").innerHTML = "";
    document.getElementById("taken-description").innerHTML = "";
    document.getElementById("taken-price").innerHTML = "";
    document.getElementById("taken-taken").innerHTML = ".";
}


//all code for posted modal
async function updatePostedModal(job_id){
    resetPostedModal();
    let json = await requestPostedPost(job_id);
    displayPostedUpdate(json, job_id);
}

const requestPostedPost = async (job_id) => {
    const response = await fetch(apiURL + '/getPostForModal/posted/' + job_id + '/');
    const json = await response.json();
    return json;
}

function displayPostedUpdate(json, job_id){
    $( "#posted-title" ).text(json["title"]);
    document.getElementById("posted-location").innerHTML = json["location"];
    document.getElementById("posted-description").innerHTML = json["description"];
    document.getElementById("posted-price").innerHTML = json["price"];
    document.getElementById("posted-provided").innerHTML = json["taken_by"];

    if (json["taken_by"] == "Nobody"){
        $( "#complete-button" ).hide();
        $( "#remove-button" ).show();
        $(" #complete-link ").attr("href", "#");
        new_url = apiURL + "/removeJob/" + job_id + "/";
        $(" #remove-link ").attr("href", new_url);
    } else {
        $( "#remove-button" ).hide();
        $( "#complete-button" ).show();   
        if (json["filled"]){
            $(" #complete-button ").hide();
        } else {
            new_url = apiURL + "/markAsComplete/" + job_id + "/";
            $(" #complete-link ").attr("href", new_url);
        }
    }
}

function resetPostedModal(){
    $( "#posted-title" ).text("Loading...");
    document.getElementById("posted-location").innerHTML = "";
    document.getElementById("posted-description").innerHTML = "";
    document.getElementById("posted-price").innerHTML = "";
    document.getElementById("posted-provided").innerHTML = ".";
}