window.onload = function() {
  this.checkAgreement();
}

function checkAgreement(){
    if (getCookie("agreed") !== "true"){
        let cookieBar = document.getElementById("cookieBar");
        cookieBar.style.display = "block";
    }
}

function closeCookieBar(){
    let cookieBar = document.getElementById("cookieBar");
    cookieBar.style.display = "none";
    document.cookie = "agreed=true";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }