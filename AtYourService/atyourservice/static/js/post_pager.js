let post = 1;
let max = 1;
const apiURL = "https://atyourservice.business";

window.onload = function() {
    initalPageLoad();
    this.checkAgreement();
}

function nextPost(){
    if (post < max){
        post++;
        updateCountUI();
        updatePagerUI();
        requestPost(); 
        updateTakePostURL();
    }
}

function prevPost(){
    if (post > 1){
        post--;
        updateCountUI();
        updatePagerUI();
        requestPost();    
        updateTakePostURL();   
    }
}

const initalPageLoad = async () => {
    setPending();
    const response = await fetch(apiURL + '/getPost/1/?max=1'); 
    const json = await response.json();
    if ("error" in json){
        setPost("No posts in your area!", "", "", "", "There are no posts in your area! Come back soon!", -1);
        $( "#post-count" ).text("0/0");
        $( "#take-job-primary" ).removeClass("btn-primary").addClass("btn-secondary");
        $( "#take-job-primary" ).attr("disabled", "");
        updatePagerUI();
        return;
    }
    max = json["max"];
    updateCountUI();
    updatePagerUI();
    parseResponse(json);
    updateTakePostURL();
}

const requestPost = async () => {
    const response = await fetch(apiURL + '/getPost/' + post + '/');
    setPending();
    const json = await response.json();
    parseResponse(json);
}

function parseResponse(response){
        response = response["post"];
        setPost(response["title"], response["area"], response["time"], "$" + response["price"], response["description"], response["id"], response["poster_id"]);
}

function updateCountUI(){
    $( "#post-count" ).text(post + "/" + max);
}

function updatePagerUI(){
    if (post >= max){
        $( "#next-post" ).removeClass("text-primary").addClass("text-light");
    } else {
        $( "#next-post" ).removeClass("text-light").addClass("text-primary");
    }
    if (post == 1){
        $("#prev-post").removeClass("text-primary").addClass("text-light");
    } else {
        $( "#prev-post" ).removeClass("text-light").addClass("text-primary");   
    }
}

function updateTakePostURL(){
    let oldURL = $("#take_job_button").attr("href");
    //gets the second occurance of the slash. I told you it was finnicky.
    let length = oldURL.length;
    currIndex = -1;
    toFind = 2;
    while(toFind-- && currIndex++ < length ){
        currIndex = oldURL.indexOf("/", currIndex);
        if (currIndex < 0) break;
    }
    let baseURL = oldURL.substring(0, currIndex + 1);
    let newURL = baseURL + post + "/";
    $("#take_job_button").attr("href", newURL);
}

function setPost(title, address, time, price, body, id, poster_id){
    $( "#job-card-title" ).text(title);
    $( "#job-card-address" ).text(address);
    $( "#job-card-time" ).text(time);
    $( "#job-card-price" ).text(price);
    $( "#job-card-body" ).text(body);
    $( "#poster-id" ).text(poster_id);
    $( "#job-id" ).text(id);
    if (parseInt($( "#current-user-id" ).text()) === poster_id){
        $( "#take-job-primary" ).removeClass("btn-primary").addClass("btn-secondary");
        $( "#take-job-primary" ).attr("disabled", "");
        $('#job-notification').text("You posted this job. you cannot take your own job.");
    } else {
        $( "#take-job-primary" ).removeClass("btn-secondary").addClass("btn-primary");
        $('#job-notification').text("");
        $( "#take-job-primary" ).removeAttr("disabled");
    }
}

function setPending(){
    setPost("loading...", "", "", "", "", -1, -1);
}